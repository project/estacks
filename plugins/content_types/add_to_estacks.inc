<?php

$plugin = array (
  'single' => TRUE,
  'icon' => 'icon_node_form.png',
  'title' => t('eStacks: Add To eStacks Link'),
  'description' => t('Provides a link to add an item to eStacks'),
  'category' => t('eStacks'),
  'render callback'  => 'estacks_add_to_stack_render',
  'edit form'        => 'estacks_add_to_stack_edit_form'
);

function estacks_add_to_stack_render($subtype, $conf, $panel_args, &$context) {

  // Add external eStacks script
  drupal_add_js(ESTACKS_API, array('type' => 'external', 'scope' => 'footer'));

  // Setup some JS settings for global JS variable. These will override what was declared in estacks_init().
  $js_settings = array(
    'divId' => 'estacks-container',
  );

  drupal_add_js(array('estacks' => $js_settings), array('type' => 'setting'));
  drupal_add_js(drupal_get_path('module', 'estacks') . '/js/estacks-global.js', array('scope' => 'footer'));

  $block = new stdClass();
  $block->title = '';

  // External script loads in the style and innerHTML for this element
  $block->content = '<a id="estacks-container" href=""></a>';
  return $block;
}

/**
 * Configuration form
 */
function estacks_add_to_stack_edit_form($form, &$form_state) {
  return $form;
}

/**
 * Configuration form submit handler
 */
function estacks_add_to_stack_edit_form_submit(&$form, &$form_state) {
}