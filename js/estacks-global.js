// Add eStacksCfg to global scope
var estacksCfg = {};
(function ($) {
  Drupal.behaviors.eStacksGlobal = {
    attach: function (context, settings) {
     
      estacksCfg = {
        pageType: "articledetails",
        platform: "drupal-jcore",
        divId: settings.estacks.divId
      };
     
     //On click event for my stacks
     $(".estacks-mystacks", context).click(function(e){
        eStacksWidget.loadMyStacks();
        e.preventDefault();
      });
    }
  }
})(jQuery);